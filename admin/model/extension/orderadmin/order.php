<?php

class ModelOrderadminOrder extends Model
{
    public function createOrder($orderData)
    {
        $this->load->model('catalog/product');

        $this->load->model('setting/setting');
        $this->settings = $this->model_setting_setting->getSetting('orderadmin');

        if (empty($orderData))
            return false;
        if (empty($this->settings['orderadmin_url']) || empty($this->settings['orderadmin_api_key']) || empty($this->settings['orderadmin_api_secret']))
            return false;

        require_once DIR_SYSTEM . 'library/orderadmin/orderadmin.php';

        $this->orderadminApi = new OrderadminAPI($this->settings['orderadmin_url'], $this->settings['orderadmin_api_key'], $this->settings['orderadmin_api_secret']);

        $res = $this->orderadminApi->request('GET', sprintf('/api/products/order'), [
            'shop' => $this->settings['orderadmin_products_shop'],
            'extId' => $orderData['order_id'],
        ])->getResult();

        if ($res['total_items'] == 0) {
            $orderTotalData = [];
            foreach ($orderData['order_total'] as $total) {
                $orderTotalData[$total['code']] = $total;
            }

            $order = [
                'shop' => $this->settings['orderadmin_products_shop'],
                'extId' => $orderData['order_id'],
                'date' => $orderData['date_added'],
                'profile' => [
                    'extId' => $orderData['customer_id'],
                    'name' => $orderData['firstname'],
                    'surname' => $orderData['lastname'],
                    'email' => $orderData['email'],
                    'phones' => [
                        [
                            'phone' => $orderData['telephone'],
                        ],
                    ],
                ],
                'address' => [
                    'postcode' => $orderData['shipping_postcode'],
                    'country' => [
                        'code' => $orderData['shipping_iso_code_2'],
                    ],
                    'locality' => [
                        'name' => $orderData['shipping_city'],
                    ],
                    'notFormal' => join(', ', array_filter([$orderData['shipping_address_1'], $orderData['shipping_address_2']])),
                ],
                'recipientName' => join(' ', array_filter([$orderData['shipping_firstname'], $orderData['shipping_lastname']])),
                'email' => $orderData['email'],
                'paymentState' => 'not_paid',
                'orderPrice' => $orderTotalData['total']['value'],
                'totalPrice' => $orderTotalData['total']['value'],
                'orderProducts' => [],
                'raw' => $orderData,
            ];

            foreach ($orderData['products'] as $product) {
                $order['orderProducts'][] = [
                    'productOffer' => [
                        'extId' => $product['product_id'],
                        'shop' => $this->settings['orderadmin_products_shop'],
                    ],
                    'count' => $product['quantity'],
                    'price' => !empty($product['price'])
                        ? $product['price'] : 0,
                    'discountPrice' => null,
                    'tax' => $product['price'] * $product['tax'],
                    'total' => $product['total'],
                ];
            }

            $res = $this->orderadminApi->setRequest($order)->request('POST', sprintf('/api/products/order'), [])->getResult();

            if (!empty($res['id'])) {
                echo '<p>Created order ' . $res['id'] . '</p>';
            } else {
                echo "<pre>";
                echo "<b>" . __FILE__ . "</b><br/>";
                if (empty($res)) {
                    $res = $this->orderadminApi->getResult(false);
                }

                var_dump($res);
                echo "</pre>";
                die();
            }
        }
    }
}
