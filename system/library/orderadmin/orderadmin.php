<?php

class OrderadminAPI
{
    const TYPE_GET = 'GET';
    const TYPE_POST = 'POST';

    protected $user;
    protected $secret;
    protected $json;
    protected $result;
    protected $server = 'https://alpha.orderadmin.ru';
    protected $timeout = 15;
    protected $error;

    public function __construct($url, $user, $secret)
    {
        $this->user = $user;
        $this->secret = $secret;
    }

    public function getResult($format = true)
    {
        if ($format) {
            return json_decode($this->result, true);
        } else {
            return $this->result;
        }
    }

    public function getError()
    {
        return $this->error;
    }

    public function setRequest($array)
    {
        $this->json = json_encode($array);

        return $this;
    }

    public function setJsonRequest($json)
    {
        $this->json = $json;

        return $this;
    }

    public function getRequest()
    {
        return $this->json;
    }

    public function request($type, $url, $query = [], $debug = false)
    {
        $ch = curl_init();

        $url = $this->server . $url;
        if (!empty($query)) {
            $url = $url . '?' . http_build_query($query);
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json',
        ));

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
        if (in_array($type, [self::TYPE_POST])) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getRequest());
        }

        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $this->user . ":" . $this->secret);

        if ($debug) {
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        }

        $this->result = curl_exec($ch);

        if ($debug) {
            var_dump(json_decode($this->getRequest(), JSON_PRETTY_PRINT + JSON_UNESCAPED_UNICODE));

            echo '<pre>';
            echo $this->result;
            print_r(curl_getinfo($ch));
            echo '</pre>';

            die();
        }

        curl_close($ch);

        return $this;
    }
}