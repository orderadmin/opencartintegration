<?php

$_['heading_title'] = 'Orderadmin';
$_['orderadmin_title'] = 'Orderadmin';

$_['text_module'] = 'Modules';
$_['text_success'] = 'Setting saved';
$_['orderadmin_base_settings'] = 'Connection settings';
$_['orderadmin_settings'] = 'Dictionary settings';
$_['orderadmin_countries_settings'] = 'Trading zones setting';

$_['orderadmin_url'] = 'Orderadmin URL';
$_['orderadmin_api_key'] = 'Orderadmin API Key';
$_['orderadmin_api_secret'] = 'Orderadmin API Secret';
$_['orderadmin_products_shop'] = 'Orderadmin shop ID';

$_['orderadmin_delivery'] = 'Shipment methods';
$_['orderadmin_status'] = 'Order statuses';
$_['orderadmin_payment'] = 'Payment methods';

$_['column_total'] = 'Total';
$_['product_summ'] = 'Amount';

$_['article'] = 'SKU';
$_['color'] = 'Color';
$_['weight'] = 'Weight';
$_['size'] = 'Size';

