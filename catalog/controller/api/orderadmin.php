<?php
class ControllerApiOrderadmin extends Controller
{
    public function authenticate ($key) {
        $this->load->model('setting/setting');
        $settings = $this->model_setting_setting->getSetting('orderadmin');

        if ($key !== $settings['orderadmin_api_key']) {
            return false;
        } else {
            return true;
        }
    }

    public function products()
    {
        if ($this->authenticate($this->request->get['key'])) {
            $json = array();

            $this->load->model('catalog/product');

            $products = $this->model_catalog_product->getProducts();
            $json['products'] = $products;

            if (isset($this->request->server['HTTP_ORIGIN'])) {
                $this->response->addHeader(
                    'Access-Control-Allow-Origin: '
                    . $this->request->server['HTTP_ORIGIN']
                );
                $this->response->addHeader(
                    'Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS'
                );
                $this->response->addHeader('Access-Control-Max-Age: 1000');
                $this->response->addHeader(
                    'Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With'
                );
            }

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        } else {
            die;
        }
    }

    public function orders()
    {
        if ($this->authenticate($this->request->get['key'])) {
            $json = array();

            $this->load->model('extension/orderadmin/sale/order');
            $this->load->model('account/order');
            $this->load->model('account/customer');
            $this->load->model('checkout/order');

            if (isset($this->request->get['limit'])
                && isset($this->request->get['start'])
            ) {
                $data = [
                    'limit' => (int)$this->request->get['limit'],
                    'start' => (int)$this->request->get['start']
                ];
                $orders
                    = $this->model_extension_orderadmin_sale_order->getOrders(
                    $data
                );
            } else {
                $orders
                    = $this->model_extension_orderadmin_sale_order->getOrders();
            }

            foreach ($orders as &$order) {

                $order = $this->model_checkout_order->getOrder(
                    $order['order_id']
                );

                if ($order['customer_id'] != '0') {
                    $customer = $this->model_account_customer->getCustomer(
                        $order['customer_id']
                    );
                } else {
                    $customer = [
                        'customer_id' => rand(1, 1000000),
                        'firstname'   => $order['firstname'],
                        'lastname'    => $order['lastname'],
                        'email'       => $order['email'],
                        'telephone'   => $order['telephone'],
                    ];
                }

                $order['account'] = $customer;
                $order['products']
                    = $this->model_account_order->getOrderProducts(
                    $order['order_id']
                );

                foreach ($order['products'] as $key => $item) {
                    $order['products'][$key]['options']
                        = $this->model_account_order->getOrderOptions(
                        $order['order_id'], $item['order_product_id']
                    );
                }
            }

            $json['orders'] = $orders;

            if (isset($this->request->server['HTTP_ORIGIN'])) {
                $this->response->addHeader(
                    'Access-Control-Allow-Origin: '
                    . $this->request->server['HTTP_ORIGIN']
                );
                $this->response->addHeader(
                    'Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS'
                );
                $this->response->addHeader('Access-Control-Max-Age: 1000');
                $this->response->addHeader(
                    'Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With'
                );
            }

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        } else {
            die;
        }
    }

    public  function order_states() {
        if ($this->authenticate($this->request->get['key']))
        {
            $json = array();
            $this->load->model('localisation/order_status');

            $orderStatuses
                = $this->model_localisation_order_status->getOrderStatuses();


            $json['orderStates'] = $orderStatuses;

            if (isset($this->request->server['HTTP_ORIGIN'])) {
                $this->response->addHeader(
                    'Access-Control-Allow-Origin: '
                    . $this->request->server['HTTP_ORIGIN']
                );
                $this->response->addHeader(
                    'Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS'
                );
                $this->response->addHeader('Access-Control-Max-Age: 1000');
                $this->response->addHeader(
                    'Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With'
                );
            }

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
        } else {
            die;
        }
    }

    public  function edit_order_state()
    {
        if ($this->authenticate($this->request->get['key'])) {

            $this->load->model('checkout/order');

            $order_id = (int)$this->request->get['order_id'];
            $order_status_id = (int)$this->request->get['order_state'];

            $this->model_checkout_order->addOrderHistory($order_id, $order_status_id);

        } else {
            die;
        }
    }
}
